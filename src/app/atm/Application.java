package app.atm;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by Hanna on 5/8/2016.
 */
public class Application {
    private static ATMMachine atmMachine = new ATMMachine(2000);

    public static void main(String[] args) {
        System.out.println("\t\t\t\t\t\t\t -----------------------");
        System.out.println("\t\t\t\t\t\t\t ****Welcome to ATM!****");
        System.out.println("\t\t\t\t\t\t\t -----------------------");

        start();
    }

    public static void start() {
        while (true) {
            System.out.println("+----------------------------------------------------------------------------------+");
            System.out.printf("|%-82s|\n", atmMachine.getCurrentState().getDescription());
            System.out.println(atmMachine.getCurrentState().getMenu());
            System.out.println("+----------------------------------------------------------------------------------+");
            Scanner sc = new Scanner(System.in);
            boolean error = false;
            //TODO move to interface and classes implementation into handleUserInput();
            do {
                try {
                    int userChoice = sc.nextInt();
                    error = false;
                    switch (userChoice) {
                        case 1:
                            atmMachine.insertCard();
                            break;
                        case 2:
                            atmMachine.ejectCard();
                            break;
                        case 3:
                            atmMachine.insertPin();
                            break;
                        case 4:
                            atmMachine.requestCash();
                            break;
                        default:
                            error = true;
                            System.out.println("Wrong input. Please try again");
                    }

                } catch (InputMismatchException e) {
                    System.out.println("Wrong input. Please try again");
                    error = true;
                    sc = new Scanner(System.in);
                }
            } while (error);

        }
    }
}
