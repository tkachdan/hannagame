package app.atm.states;

import app.atm.ATMMachine;
import app.atm.ATMState;

/**
 * Created by Hanna on 5/8/2016.
 */
public class NoCardState implements ATMState {

    ATMMachine atmMachine;

    public NoCardState(ATMMachine atmMachine) {
        this.atmMachine = atmMachine;
    }

    @Override
    public void insertCard() {
        System.out.println("Card has been inserted");
        atmMachine.setCurrentState(atmMachine.getHasCardSate());
    }

    @Override
    public void requestCash() {
        System.out.println("You have not inserted your card. Please, insert a card before proceeding");
    }

    @Override
    public void ejectCard() {
        System.out.println("You have not inserted your card. Please, insert a card before proceeding");
    }

    @Override
    public void insertPin() {
        System.out.println("You have not inserted your card. Please, insert a card before proceeding");
    }

    @Override
    public String getDescription() {
        return "State: No card inserted";
    }

    @Override
    public String getMenu() {
        String menu = "|Choose your option: 1. Insert card, 2. Eject card, 3. Insert PIN, 4. Request cache|";

        return menu;
    }
}
