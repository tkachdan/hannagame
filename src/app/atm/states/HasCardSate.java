package app.atm.states;

import app.atm.ATMMachine;
import app.atm.ATMState;

/**
 * Created by Hanna on 5/8/2016.
 */
public class HasCardSate implements ATMState {
    ATMMachine atmMachine;

    public HasCardSate(ATMMachine atmMachine) {
        this.atmMachine = atmMachine;
    }

    @Override
    public void insertCard() {
        System.out.println("You`ve already insert your card");
    }

    @Override
    public void requestCash() {
        System.out.println("enter pin");
    }

    @Override
    public void ejectCard() {
        System.out.println("bye bye");
        atmMachine.setCurrentState(atmMachine.getNoCardState());
    }

    @Override
    public void insertPin() {
        System.out.println("pin ok");
        atmMachine.setCurrentState(atmMachine.getHasPinState());

    }

    @Override
    public String getDescription() {
        return "State: has card";
    }

    @Override
    public String getMenu() {
        return "|Choose your option: 1. Eject card, 3. Insert PIN, 4. Request cache|";
    }
}
