package app.atm.states;

import app.atm.ATMMachine;
import app.atm.ATMState;

/**
 * Created by Hanna on 5/8/2016.
 */
public class NoCashState implements ATMState {
    ATMMachine atmMachine;

    public NoCashState(ATMMachine atmMachine) {
        this.atmMachine = atmMachine;
    }

    @Override
    public void insertCard() {
        System.out.println("Sorry, no cash");
    }

    @Override
    public void requestCash() {
        System.out.println("Sorry, no cash");
    }

    @Override
    public void ejectCard() {
        System.out.println("Sorry, no cash");
    }

    @Override
    public void insertPin() {
        System.out.println("Sorry, no cash");
    }

    @Override
    public String getDescription() {
        return "State: No cash State";
    }

    @Override
    public String getMenu() {
        return null;
    }
}
