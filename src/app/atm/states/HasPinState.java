package app.atm.states;

import app.atm.ATMMachine;
import app.atm.ATMState;

/**
 * Created by Hanna on 5/8/2016.
 */
public class HasPinState implements ATMState {
    ATMMachine atmMachine;

    public HasPinState(ATMMachine atmMachine) {
        this.atmMachine = atmMachine;
    }

    @Override
    public void insertCard() {
        System.out.println("Card is already inserted");
    }

    @Override
    public void requestCash() {
        System.out.println("Select amount");
        int amount = 100;
        if(amount > atmMachine.getCashInMachine()){
            atmMachine.setCurrentState(atmMachine.getNoCashState());
        }
        System.out.println("You have withdrawn: "+ amount + " CZK");
        atmMachine.setCashInMachine(atmMachine.getCashInMachine() - amount);
        atmMachine.setCurrentState(atmMachine.getNoCardState());
    }

    @Override
    public void ejectCard() {
        System.out.println("Bye bye");
        atmMachine.setCurrentState(atmMachine.getHasPinState());
    }

    @Override
    public void insertPin() {
        System.out.println("You have already entered your pin");
    }

    @Override
    public String getDescription() {
        return "State: has pin";
    }

    @Override
    public String getMenu() {
        return null;
    }
}
