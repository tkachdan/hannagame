package app.atm;

import app.atm.states.HasCardSate;
import app.atm.states.HasPinState;
import app.atm.states.NoCardState;
import app.atm.states.NoCashState;

/**
 * Created by Hanna on 5/8/2016.
 */
public class ATMMachine {
    private HasCardSate hasCardSate;
    private HasPinState hasPinState;
    private NoCardState noCardState;
    private NoCashState noCashState;

    private ATMState currentState;

    private int cashInMachine = 1000;

    public ATMMachine(int cashInMachine) {
        hasCardSate = new HasCardSate(this);
        hasPinState = new HasPinState(this);
        noCardState = new NoCardState(this);
        noCashState = new NoCashState(this);

        this.cashInMachine = cashInMachine;

        this.currentState = noCardState;
    }


    public HasCardSate getHasCardSate() {
        return hasCardSate;
    }

    public void setHasCardSate(HasCardSate hasCardSate) {
        this.hasCardSate = hasCardSate;
    }

    public HasPinState getHasPinState() {
        return hasPinState;
    }

    public void setHasPinState(HasPinState hasPinState) {
        this.hasPinState = hasPinState;
    }

    public NoCardState getNoCardState() {
        return noCardState;
    }

    public void setNoCardState(NoCardState noCardState) {
        this.noCardState = noCardState;
    }

    public NoCashState getNoCashState() {
        return noCashState;
    }

    public void setNoCashState(NoCashState noCashState) {
        this.noCashState = noCashState;
    }

    public ATMState getCurrentState() {
        return currentState;
    }

    public void setCurrentState(ATMState currentState) {
        this.currentState = currentState;
    }

    public int getCashInMachine() {
        return cashInMachine;
    }

    public void setCashInMachine(int cashInMachine) {
        this.cashInMachine = cashInMachine;
    }

    public void insertCard() {
        this.currentState.insertCard();
    }

    public void requestCash() {
        this.currentState.requestCash();
    }

    public void ejectCard() {
        this.currentState.ejectCard();
    }

    public void insertPin() {
        this.currentState.insertPin();
    }

    public String getDescription() {
        return this.currentState.getDescription();
    }
}
