package app.atm;

/**
 * Created by Hanna on 5/8/2016.
 */

public interface ATMState {

    void insertCard();

    void requestCash();

    void ejectCard();

    void insertPin();

    String getDescription();

    String getMenu();
}
