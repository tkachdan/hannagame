package app.hra;

import app.hra.locations.Location;


/**
 * Created by Hanna on 5/8/2016.
 */
public class Character implements CharacterState{


    public String name;
    public String lastName;
    public String role;

    public Location location;

    public Character(String name, String lastName, String role, Location location) {
        this.name = name;
        this.lastName = lastName;
        this.role = role;
        this.location = location;

    }

    public Location getLocation() {
        return location;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public void goTo(Location location) {
        this.location = location;
    }

    @Override
    public void talkTo() {
        System.out.println("youve talked");
    }

    @Override
    public void napoveda() {
        System.out.println("Je to rok 1947. Soud vyslechl případ proti Vám, viceprezidentu banky Andy Dyufreyna, z vraždy své \\n manželky a jejího milence. Vy neuznáváte svou vinu a dokonce i nepamatuje si, co jste dělal tehdy, \\n protože jste byl opilý. Krátce před vraždou jste chytil svou ženu s jiným mužem, ale žádost manželky o \\n rozvod jste odmítl. Žena opustila dům tentýž večer. Šel jste do domu, kam vaše žena odešla, ale nikdo \\n tam nebyl, a tak jste se rozhodl počkat na ni a jejího milence. S sebou jste měl pistoli. Po nějakém čase \\n jste  šel domů a ráno služka našla vaši manželku s milencem zastřeleni. Zbraň, kterou jste na cestě \\n domů hodil do řeky, policie nenašla. Soud Vás odsoudil ke dvěma doživotním trestům. Jdete do \\n Šoušenku. Postupně budete sbírat informace, které Vám pak pomohou utéct.  Hráč hru vyhrává \\n v momentě, když je mimo vezen. Během hry mohou nastat případy, kdy bude zraněn nebo zabit. To \\n znamená, že hráč prohrál. ");
    }


    @Override
    public void konec() {
        System.exit(0);
    }

    @Override
    public String pomoc() {
        return null;
    }

    @Override
    public void read() {

    }

    @Override
    public void digWall() {

    }

    @Override
    public void teachTom() {

    }

    @Override
    public void doFinancialStuff() {

    }

    @Override
    public void eat() {

    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public void give() {

    }

    @Override
    public void take() {

    }
}
