package app.hra.locations;

/**
 * Created by Hanna on 5/8/2016.
 */
public class Location {

    public String name;

    public Location(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Location{" +
                "name='" + name + '\'' +
                '}';
    }
}
