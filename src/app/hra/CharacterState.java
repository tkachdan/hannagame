package app.hra;

import app.hra.locations.Location;

/**
 * Created by Hanna on 5/8/2016.
 */
public interface CharacterState {

    void goTo(Location location);

    void talkTo();

    void napoveda();

    void konec();

    String pomoc();

    void read();

    void digWall();

    void teachTom();

    void doFinancialStuff();

    void eat();

    void give();

    void take();


}
