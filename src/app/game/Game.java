package app.game;

import app.game.models.Person;
import app.game.services.GameService;
import app.game.services.MapService;

import java.util.Scanner;

/**
 * Created by danylo.tkachenko on 10/05/16.
 */
public class Game {
    GameService gameService;
    MapService mapService;

    public static void main(String[] args) {
        new Game().init();
    }

    private void init() {
        Person person = new Person("Main person");
        gameService = new GameService(person);
        mapService = new MapService(gameService);
        mapService.setupMap(person);


        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("You are at: " + person.getCurrentLocation().getName());
            System.out.println(person.getCurrentLocation().getDescription());
            System.out.println("=================================");
            gameService.printConnectedLocations();
            gameService.printOptions();
            System.out.println("Choose option:");

            int chosenOption = scanner.nextInt();
            gameService.handleOptionSelection(chosenOption);

            /*int nextLocation = scanner.nextInt();
            gameService.movePersonToLocation(nextLocation);*/
        }
    }


}
