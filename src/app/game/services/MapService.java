package app.game.services;

import app.game.models.Location;
import app.game.models.Option;
import app.game.models.OptionType;
import app.game.models.Person;

/**
 * Created by danylo.tkachenko on 11/05/16.
 */
public class MapService {
    GameService mainService;

    public MapService(GameService mainService) {
        this.mainService = mainService;
    }

    public void setupMap(Person person) {
        Location location = new Location("meadow", "Description");
        mainService.addOption(location, new Option("Move", OptionType.MOVE));
        mainService.addOption(location, new Option("Pick item", OptionType.FIGHT));

        Location location1 = new Location("battlefield", "Description");
        mainService.addOption(location1, new Option("Move", OptionType.MOVE));
        mainService.addOption(location1, new Option("fight", OptionType.FIGHT));

        mainService.connectToLocation(location, location1);
        mainService.connectToLocation(location1, location);

        Location location2 = new Location("udoli kamenu", "Description");
        mainService.addOption(location2, new Option("play", OptionType.PLAY));
        mainService.addOption(location2, new Option("pick", OptionType.PICK));

        mainService.connectToLocation(location1, location2);
        mainService.connectToLocation(location2, location1);


        Location location3 = new Location("bron", "Description");
        mainService.addOption(location3, new Option("Move", OptionType.MOVE));
        mainService.addOption(location3, new Option("fight", OptionType.FIGHT));

        mainService.connectToLocation(location, location3);
        mainService.connectToLocation(location3, location);

        Location location4 = new Location("dragon", "Description");
        mainService.addOption(location4, new Option("fight", OptionType.FIGHT));
        mainService.addOption(location4, new Option("Move", OptionType.MOVE));

        mainService.connectToLocation(location3, location4);
        mainService.connectToLocation(location4, location3);

        Location location5 = new Location("gate", "Description");
        mainService.addOption(location5, new Option("open the gate", OptionType.OPEN));
        mainService.addOption(location5, new Option("Move", OptionType.MOVE));

        mainService.connectToLocation(location4, location5);
        mainService.connectToLocation(location5, location4);

        Location location6 = new Location("princess", "Description");
        mainService.addOption(location5, new Option("rescue", OptionType.RESCUE));

        mainService.connectToLocation(location5, location6);
        mainService.connectToLocation(location6, location5);

        person.setCurrentLocation(location);

        System.out.println(mainService.getStartDescription());
    }
}
