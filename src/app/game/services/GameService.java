package app.game.services;

import app.game.models.Location;
import app.game.models.Option;
import app.game.models.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by danylo.tkachenko on 10/05/16.
 */
public class GameService {
    Person person;
    Scanner sc = new Scanner(System.in);


    public GameService(Person person) {
        this.person = person;
    }

    public String getStartDescription() {
        String description = "================Game================\n" +
                "Welcome to the Game v0.0.0\n" +
                "story....\n" +
                "====================================";
        return description;
    }

    public void movePersonToLocation(Location location) {
        person.setCurrentLocation(location);
    }

    public void printConnectedlocations(Location location) {
        for (Location location1 : location.getConnectedLocations()) {

        }
    }

    public void printOptions() {
        System.out.println("Options:");
        List<Option> options = person.getCurrentLocation().getOptions();
        for (int i = 0; i < options.size(); i++) {
            Option option = options.get(i);
            System.out.println(i + "." + option.getName());
        }
        System.out.println("=======================");

    }

    public void connectToLocation(Location location, Location connectToLocation) {
        List<Location> locationsList = location.getConnectedLocations();
        if (locationsList == null) {
            locationsList = new ArrayList<>();
            locationsList.add(connectToLocation);
            location.setConnectedLocations(locationsList);
        } else {
            locationsList.add(connectToLocation);
            location.setConnectedLocations(locationsList);
        }

    }

    public void addOption(Location location, Option option) {
        List<Option> options = location.getOptions();
        if (options == null) {
            options = new ArrayList<>();
            options.add(option);
            location.setOptions(options);
        } else {
            options.add(option);
            location.setOptions(options);
        }
    }

    public void movePersonToLocation(int locationIndex) {
        person.setCurrentLocation(person.getCurrentLocation().getConnectedLocations().get(locationIndex));
    }

    public void printConnectedLocations() {
        List<Location> connectedLocations = person.getCurrentLocation().getConnectedLocations();
        if (connectedLocations.size() > 0) {
            System.out.println("Connected Locations:");
            for (int i = 0; i < connectedLocations.size(); i++) {
                Location location = connectedLocations.get(i);
                System.out.println(i + "." + location.getName());

            }
            System.out.println("======================");
        }
    }

    public void handleOptionSelection(int chosenOption) {
        Option option = person.getCurrentLocation().getOptions().get(chosenOption);
        switch (option.getOptionType()) {
            case MOVE:
                hadleMove();
                break;
            case DESCRIPTION:
                break;
            case FIGHT:
                break;
        }

    }

    private void hadleMove() {
        System.out.println("Enter destination location id");
        int destination = sc.nextInt();
        movePersonToLocation(destination);
    }
}
