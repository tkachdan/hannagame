package app.game.models;

/**
 * Created by danylo.tkachenko on 12/05/16.
 */
public enum OptionType {
    MOVE,
    PICK,
    FIGHT,
    PLAY, OPEN, RESCUE, DESCRIPTION

}
