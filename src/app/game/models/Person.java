package app.game.models;

import java.util.Set;

/**
 * Created by danylo.tkachenko on 10/05/16.
 */
public class Person {
    private String name;
    private Set<Item> items;
    private Location currentLocation;


    public Location getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(Location currentLocation) {
        this.currentLocation = currentLocation;
    }

    public Person(String name) {
        this.setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Item> getItems() {
        return items;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }
}
