package app.game.models;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by danylo.tkachenko on 10/05/16.
 */
public class Location {
    private String name;
    private String description;
    private List<Location> connectedLocations = new ArrayList<>();
    private List<Option> options = new ArrayList<>();

    private Set<Item> items = new HashSet<>();

    public Location(String name, String description) {
        this.setName(name);
        this.setDescription(description);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

    public Set<Item> getItems() {
        return items;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }

    public List<Location> getConnectedLocations() {
        return connectedLocations;
    }

    public void setConnectedLocations(List<Location> connectedLocations) {
        this.connectedLocations = connectedLocations;
    }
}
