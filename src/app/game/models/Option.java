package app.game.models;

/**
 * Created by danylo.tkachenko on 10/05/16.
 */
public class Option {
    private String name;
    private OptionType optionType;

    public Option(String name, OptionType optionType) {
        this.name = name;
        this.optionType = optionType;
    }

    public OptionType getOptionType() {
        return optionType;
    }

    public void setOptionType(OptionType optionType) {
        this.optionType = optionType;
    }

    public Option(String name) {
        this.setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
